import time
import os
import tqdm
import socket
from utility import wait_for_acknowledge

BUFFERSIZE = 4096
HEADERSIZE = 1024
SERVER = socket.gethostbyname(socket.gethostname())
PORT = 5050
WORKER1 = socket.gethostbyaddr("ec2-3-84-238-133.compute-1.amazonaws.com")[0]
PORT_W1 = 5051
ADDR1 = (WORKER1, PORT_W1)
WORKER2 = socket.gethostbyaddr("ec2-54-165-215-23.compute-1.amazonaws.com")[0]
PORT_W2 = 5050
ADDR2 = (WORKER2, PORT_W2)


# SERVER = socket.gethostbyaddr("ec2-54-165-215-23.compute-1.amazonaws.com")[0]

SEPARATOR = "<SEPARATOR>"
DISCONNECT_MESSAGE = "DISCONNECT"


# master.settimeout(5)  # limit each communication time to 5s


def sendImage(file_name, master):
    file_size = os.path.getsize(file_name)
    print(f"file prop: {file_name}{SEPARATOR}{file_size}")
    master.send(
        f"{file_name}{SEPARATOR}{file_size}{SEPARATOR}img".encode('utf-8'))
    check_received(master, "worker 3")
    progress = tqdm.tqdm(desc=f'Sending {file_name}', total=file_size, unit="B",
                         unit_scale=True, unit_divisor=1024)
    progress_counter = 0
    with open(file_name, "rb") as f:
        while progress_counter != file_size:
            bytes_read = f.read(BUFFERSIZE)
            master.sendall(bytes_read)
            check_received(master, "worker 3")
            progress_counter += len(bytes_read)
            progress.update(len(bytes_read))
    progress.close()


def sendText(file_name, text, master):
    master.send(
        f"{file_name}{SEPARATOR}{len(text)}{SEPARATOR}text".encode('utf-8'))
    check_received(master, "worker 3")
    master.send(text.encode('utf-8'))
    check_received(master, "worker 3")


def count_alphabet(master):
    kata = str(input("Masukkan Apapun Kalimat : "))
    panjang_kata = len(kata)
    master.sendall(bytes(str(panjang_kata) ,'utf-8'))
    master.sendall(bytes(kata, 'utf-8'))

    length_res = master.recv(HEADERSIZE).decode('utf-8')
    if length_res:
        length_res = int(length_res) 

    hasil = wait_for_acknowledge(master, length_res)
    print(f"HASIL PENGHITUNGAN WORKER : \n{hasil}")
    



def receiveImage(master):
    file_decode = master.recv(HEADERSIZE).decode('utf-8')
    print(file_decode)
    if file_decode:
        master.send(f"received".encode('utf-8'))
    file_name, file_size = file_decode.split(SEPARATOR)
    file_name = os.path.basename(file_name)
    file_size = int(file_size)
    progress_counter = 0
    progress = tqdm.tqdm(
        desc=f'Receiving {file_name}', total=file_size, unit="B", unit_scale=True, unit_divisor=1024)
    with open(f"./img_received/{file_name}", "wb") as f:
        while progress_counter != file_size:
            bytes_read = master.recv(BUFFERSIZE)
            if bytes_read:
                master.send(f"received".encode('utf-8'))
            f.write(bytes_read)
            progress_counter += len(bytes_read)
            progress.update(len(bytes_read))
    progress.close()


def close_socket(master):
    master.send(DISCONNECT_MESSAGE.encode('utf-8'))
    master.close()


def kill_worker(master):
    master.send("die".encode('utf-8'))
    master.close()


def check_received(socket, type):
    confirm = socket.recv(HEADERSIZE)
    confirm = confirm.decode("utf-8")
    # confirm = wait_for_acknowledge(socket, "received")
    print(confirm)
    # if confirm != "received":
    #     raise ValueError(f"{type} not receive data")

def send(socket, file_name):
    file_size = os.path.getsize(file_name)
    socket.send(f"{file_name}{SEPARATOR}{file_size}".encode('utf-8'))
    check_received(socket, "master")
    progress = tqdm.tqdm(desc=f'Sending {file_name}', total=file_size, unit="B",
                         unit_scale=True, unit_divisor=1024)
    progress_counter = 0
    with open(f"{file_name}", "rb") as f:
        while progress_counter != file_size:
            bytes_read = f.read(BUFFERSIZE)
            socket.sendall(bytes_read)
            check_received(socket, "master")
            progress_counter += len(bytes_read)
            progress.update(len(bytes_read))
    progress.close()


def main():
    print("Selamat Datang di Master\n\n")    
    while True:
        master = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        choose_job = int(input("Pilih job yang ingin dikerjakan [Input Angka]:\n1. Manage Image\n2. Count Alphabet\n3. Disconnect\n\n"))

        if choose_job == int('1'):
            master.connect(ADDR1)
            while True:
                choose_activity = int(input("Pilih aktivitas yang ingin dilakukan [INPUT ANGKA]:\n1. Convert JPG to PNG\n2. Creat Wordcloud\n3. Disconnect\n\n"))
                if choose_activity == int('1'):
                    file_name = "./img/basdat.jpg"
                    sendImage(file_name, master)
                    receiveImage(master)
                elif choose_activity == int('2'):
                    nama_image = str(input("Masukkan nama image yang ingin dibuat [tanpa extension] : "))
                    tulisan = str(input("Masukkan tulisan yang ingin dicetak pada image : "))
                    sendText(f"{nama_image}.jpg", f'{tulisan}', master)
                    receiveImage(master)
                elif choose_activity == int('3'):
                    kill_worker(master)
                    break
        elif choose_job == int('2'):
            master.connect(ADDR2)
            while True:
                choose_activity = int(input("Pilih aktivitas yang ingin dilakukan [INPUT ANGKA]:\n1. Hitung Jumlah Huruf\n2. Disconnect\n\n"))
                if choose_activity == int('1'):
                    count_alphabet(master)
                elif choose_activity == int('2'):
                    kill_worker(master)
                    break
        elif choose_job == int('3'):
            print("[MASTER CLOSING CONNECTION]")
            master.close()
            break
    
    print("[SOCKET PROGRAMMING FINISHED]")


if __name__ == "__main__":
    main()
