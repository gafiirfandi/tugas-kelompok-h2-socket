import os
import tqdm
import time
import threading
import socket
from PIL import Image
from wordcloud import WordCloud

BUFFERSIZE = 4096
HEADERSIZE = 1024
PORT = 5001
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
SEPARATOR = "<SEPARATOR>"
DISCONNECT_MESSAGE = "DISCONNECT"

worker3 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
worker3.bind(ADDR)


def handle_client(master_socket, addr, listen):
    print(f"[NEW CONNECTIONS] {addr} connected.")
    while True:
        msg_decode = master_socket.recv(HEADERSIZE).decode('utf-8')
        if msg_decode:            
            master_socket.send(f"received".encode('utf-8'))
        if msg_decode == DISCONNECT_MESSAGE:
            close_master(master_socket)
            print(f"Master {addr} Disconnect")
            break
        elif msg_decode == "die":
            listen[0] = False
            close_master(master_socket)
            print(f"Byebye")
            break
        msg_splitted = msg_decode.split(SEPARATOR)
        print(f'msg_splitted: {msg_decode}')
        if(len(msg_splitted) != 0):
            if msg_splitted[2] == "text":
                msg_len = int(msg_splitted[1])
                text_to_process = receiveText(master_socket, msg_len)
                print(text_to_process)
                word_image(msg_splitted[0], text_to_process)
                send(master_socket, msg_splitted[0])
            else:
                file_name = os.path.basename(msg_splitted[0])
                file_size = int(msg_splitted[1])
                receiveImage(master_socket, file_name, file_size)
                file_name_converted = jpg_to_png(file_name)
                send(master_socket, file_name_converted)


def receiveText(socket, msg_len):
    full_msg = ''
    while True:
        msg = socket.recv(BUFFERSIZE)
        full_msg += msg.decode("utf-8")
        if len(full_msg) >= msg_len:
            socket.send(f"received".encode('utf-8'))
            print(full_msg)
            return full_msg


def receiveImage(socket, file_name, file_size):
    progress_counter = 0
    progress = tqdm.tqdm(
        desc=f'Receiving {file_name}', total=file_size, unit="B", unit_scale=True, unit_divisor=1024)
    with open(f"./img_received_worker/{file_name}", "wb") as f:
        while progress_counter != file_size:
            bytes_read = socket.recv(BUFFERSIZE)
            if bytes_read:
                socket.send(f"received".encode('utf-8'))
            f.write(bytes_read)
            progress_counter += len(bytes_read)
            progress.update(len(bytes_read))
    progress.close()


def jpg_to_png(file_name):
    image_convert = Image.open(f"./img_received_worker/{file_name}")
    file_name_converted = f'./img_received_worker/{file_name[:-4]}.png'
    image_convert.save(file_name_converted)
    return file_name_converted


def word_image(file_name, text):
    print(f'text_di_word_image: {text}')
    wordcloud = WordCloud(background_color='white').generate(text)
    wordcloud.to_file(file_name)


def send(socket, file_name):
    file_size = os.path.getsize(file_name)
    socket.send(f"{file_name}{SEPARATOR}{file_size}".encode('utf-8'))
    check_received(socket, "master")
    progress = tqdm.tqdm(desc=f'Sending {file_name}', total=file_size, unit="B",
                         unit_scale=True, unit_divisor=1024)
    progress_counter = 0
    with open(f"{file_name}", "rb") as f:
        while progress_counter != file_size:
            bytes_read = f.read(BUFFERSIZE)
            socket.sendall(bytes_read)
            check_received(socket, "master")
            progress_counter += len(bytes_read)
            progress.update(len(bytes_read))
    progress.close()


def check_received(socket, type):
    confirm = socket.recv(HEADERSIZE)
    confirm = confirm.decode("utf-8")
    # if confirm != "received":
    #     raise ValueError(f"{type} not receive data")


def close_master(socket):
    socket.close()


def start():
    worker3.listen(5)
    print(f"[LISTENING] server is listening on {SERVER}")
    listen = [True]
    while listen[0]:
        master_socket, addr = worker3.accept()
        # handle_client(master_socket, addr, listen)
        thread = threading.Thread(
            target=handle_client, args=(master_socket, addr, listen))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")


print("[STARTING] server is starting...")
start()
