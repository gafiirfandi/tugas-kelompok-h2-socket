print("Socket programming!")
import socket
import threading
import time
import os
from os import listdir
from utility import wait_for_acknowledge
import json

BUFFER_SIZE = 1024
PORT = 5002
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = "utf-8"
DISCONNECT = "!DISCONNECT"
SEPARATOR = "<SEPARATOR>"

workers = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
workers.bind(ADDR)

dict_aplhabet = {

    "a":0,
    "b":0,
    "c":0,
    "d":0,
    "e":0,
    "f":0,
    "g":0,
    "h":0,
    "i":0,
    "j":0,
    "k":0,
    "l":0,
    "m":0,
    "n":0,
    "o":0,
    "p":0,
    "q":0,
    "r":0,
    "s":0,
    "t":0,
    "u":0,
    "v":0,
    "w":0,
    "x":0,
    "y":0,
    "z":0,

}


def handle_count_word(conn, addr, listen):
    print(f"[NEW CONNECTIONS] {addr} connected.\n")
    connected = True
    while connected:
        
        panjang_kata = wait_for_acknowledge(conn,str("3"))
        print(panjang_kata)

        kata = wait_for_acknowledge(conn,panjang_kata)
        print(kata)

        

        for huruf in dict_aplhabet:
            count = 0
            for huruf_target in kata:
                if huruf == huruf_target.lower():
                    count += 1
                    dict_aplhabet[huruf] = count

        result = str(dict_aplhabet)
        send_length = str(len(result)).encode(FORMAT)
        send_length += b' ' * (BUFFER_SIZE - len(send_length))
        conn.send(send_length)
        conn.sendall(bytes(result, FORMAT))
        
    conn.close()
        

def close_master(socket):
    socket.close()



def start():
    workers.listen(5)
    print(f"[LISTENING] server is listening on {SERVER}")
    listen = [True]
    while listen[0]:
        master_socket, addr = workers.accept()
        thread = threading.Thread(
            target=handle_count_word, args=(master_socket, addr, listen))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")
        # handle_count_word(master_socket,addr,listen)


print("[STARTING] Worker is starting...")
start()